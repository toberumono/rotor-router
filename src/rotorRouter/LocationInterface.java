package rotorRouter;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import fileManager.NativeLoader;

public class LocationInterface extends Thread {
	static {
		NativeLoader.loadNatives("LocationInterface");
	}
	private Location location;
	private int count = 0, cacheLimit;
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private boolean stop;
	
	@Override
	public void run() {
		while (!stop) {
			while (!stop && count < cacheLimit) {
				Location temp = location;
				location = new Location(walk());
				temp.setNext(location);
			}
		}
	}
	
	public LocationInterface(int cacheLimit) {
		this.cacheLimit = cacheLimit;
		init();
		setDirections(new short[]{1, 2, 3, 4});
		location = new Location(walk());
		stop = false;
	}
	
	private native void init();
	
	public native void setDirections(short[] dirs);
	
	public native short[] getDirections();
	
	private native long[] walk();
	
	private native long[] step();
	
	private native int hitCount(long x, long y);
	
	private native int direction(long x, long y);
	
	public void countPP() {
		try {
			lock.writeLock().lock();
			count++;
		}
		finally {
			lock.writeLock().unlock();
		}
	}
	
	public void countMM() {
		try {
			lock.writeLock().lock();
			count--;
		}
		finally {
			lock.writeLock().unlock();
		}
	}
	
	public int getCount() {
		int count;
		try {
			lock.readLock().lock();
			count = this.count;
		}
		finally {
			lock.readLock().unlock();
		}
		return count;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void stopLoading() {
		stop = true;
	}
}
