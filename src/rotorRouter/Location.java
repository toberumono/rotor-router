package rotorRouter;

public class Location {
	private Location next;
	private boolean hasNext;
	private long[] loc;
	
	public Location(long[] loc) {
		this.loc = loc;
		hasNext = false;
		next = null;
	}
	
	public boolean hasNext() {
		return hasNext;
	}
	
	public Location getNext() {
		return next;
	}
	
	public void setNext(Location next) {
		this.next = next;
		hasNext = true;
	}
	
	public long[] getLoc() {
		return loc;
	}
}
