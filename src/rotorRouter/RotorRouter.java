package rotorRouter;

import java.util.Scanner;

public class RotorRouter extends Thread {
	private boolean stop;
	private Location location;
	
	public RotorRouter() {
		stop = false;
	}
	
	private void runProgram() {
		final LocationInterface li = new LocationInterface(100);
		location = li.getLocation();
		li.start();
		start();
		Scanner stopCheck = new Scanner(System.in);
		while (!(stopCheck.nextLine().equals("stop")));
		stopCheck.close();
		stop = true;
		li.stopLoading();
	}
	
	@Override
	public void run() {
		while (!stop) {
			while (!stop && location.hasNext()) {
				
			}
		}
	}
	
	public static void main(String[] args) {
		RotorRouter control = new RotorRouter();
		control.runProgram();
	}
}
