#include <stdlib.h>
#include <stdio.h>

#include "backend.h"

long int ***grid;
long long int grid_n, center;
//1 = up, 2 = right, 3 = down, 4 = left
int *dirs, num_dirs;
long long int *current_step = (long long int *) NULL;

/*int main(int argc, char **argv) {
	malloc_grid(50);
	int new_dirs[] = {1, 2, 3, 4};
	set_dirs(new_dirs, 4);
	for (int i = 0; i < 5000; i++)
		walk();
	printf("%lld\n", center);
	printf("\n");
	print_grid();
	free_grid();
	return 0;
}*/

int set_dirs(int *new_dirs, int new_num_dirs) {
	num_dirs = new_num_dirs + 1;
	dirs = (int *) malloc(num_dirs * sizeof(int));
	for (int i = 0; i < num_dirs; i++)
		dirs[i + 1] = new_dirs[i];
	return 0;
}

int *get_dirs() {
	return dirs;
}

int get_num_dirs() {
	return num_dirs;
}

int print_grid() {
	for (int i = 0; i < grid_n; i++) {
		for (int j = 0; j < grid_n; j++)
			printf(" %ld", grid[i][j][0]);
		printf("\n");
	}
	return 0;
}

int malloc_grid(long long int n) {
	grid = (long int ***) malloc(n * sizeof(long int **));
	for (long long int i = 0; i < n; i++) {
		grid[i] = (long int **) malloc(n * sizeof(long int *));
		for (long long int j = 0; j < n; j++) {
			grid[i][j] = (long int *) malloc (2 * sizeof(long int));
			grid[i][j][0] = 0;
			grid[i][j][1] = 0;
		}
	}
	grid_n = (* (long long int *) malloc(sizeof(long long int)));
	center = (* (long long int *) malloc(sizeof(long long int)));
	grid_n = n;
	center = grid_n / 2;
	return 0;
}

int free_grid() {
	for (int i = 0; i < grid_n; i++) {
		for (int j = 0; j < grid_n; j++)
			free(grid[i][j]);
		free(grid[i]);
	}
	free(grid);
	return 0;
}

int double_grid() {
	long long int old_n = grid_n;
	grid_n *= 2;
	grid = (long int ***) realloc(grid, grid_n * sizeof(long int **));
	for (long long int i = old_n + center; i < grid_n; i++) {
		grid[i] = (long int **) malloc(grid_n * sizeof(long int *));
		for (long long int j = 0; j < grid_n; j++) {
			grid[i][j] = (long int *) malloc (2 * sizeof(long int));
			grid[i][j][0] = 0;
			grid[i][j][1] = 0;
		}
	}
	for (long long int i = old_n - 1; i >= 0; i--) {
		long long int j = i + center;
		grid[j] = (long int **) realloc(grid[i], grid_n * sizeof(long int *));
		for (long long int k = old_n + center; k < grid_n; k++) {
			grid[j][k] = (long int *) malloc (2 * sizeof(long int));
			grid[j][k][0] = 0;
			grid[j][k][1] = 0;
		}
		for (long long int k = old_n - 1; k >= 0; k--)
			grid[j][k + center] = grid[j][k];
		for (long long int k = 0; k < center; k++) {
			grid[j][k] = (long int *) malloc (2 * sizeof(long int));
			grid[j][k][0] = 0;
			grid[j][k][1] = 0;
		}
	}
	for (long long int i = 0; i < center; i++) {
		grid[i] = (long int **) malloc(grid_n * sizeof(long int *));
		for (long long int j = 0; j < grid_n; j++) {
			grid[i][j] = (long int *) malloc (2 * sizeof(long int));
			grid[i][j][0] = 0;
			grid[i][j][1] = 0;
		}
	}
	center *= 2;
	return 0;
}

//Performs a complete walk
long long int *walk() {
	long long int *result = (long long int *) malloc(2 * sizeof(long long int));
	if (current_step != NULL) {
		result[0] = current_step[0];
		result[1] = current_step[1];
	}
	else {
		result[0] = center;
		result[1] = center;
	}
	long long int *temp;
	while (!(temp = step(result[0], result[1]))[2]) {
		result[0] = temp[0];
		result[1] = temp[1];
		free(temp);
	}
	result[0] = temp[0];
	result[1] = temp[1];
	free(temp);
	return result;
}

//Performs one step in a walk with the most recent step ending at (x, y)
//return new int[]{new_x, new_y, done (0 = false, 1 = true)};
long long int *step(long long int x, long long int y) {
	long long int *result = (long long int *) malloc(3 * sizeof(long long int));
	result[0] = x;
	result[1] = y;
	result[2] = 0ll;
	if (!grid[x][y][0]) {
		grid[x][y][0] = 1;
		grid[x][y][1] = 1;
		result[2] = 1ll;
		current_step = (long long int *) NULL;
		return result;
	}
	if (result[0] > grid_n - 3 || result[0] < 3 || result[1] > grid_n - 3 || result[1] < 3) {
		result[0] += center;
		result[1] += center;
		double_grid();
	}
	grid[result[0]][result[1]][0]++;
	grid[result[0]][result[1]][1]++;
	if (grid[result[0]][result[1]][0] % num_dirs == 0)
		grid[result[0]][result[1]][0] = 1;
	if (dirs[grid[result[0]][result[1]][0]] == 1)
		result[1]++;
	else if (dirs[grid[result[0]][result[1]][0]] == 2)
		result[0]++;
	else if (dirs[grid[result[0]][result[1]][0]] == 3)
		result[1]--;
	else if (dirs[grid[result[0]][result[1]][0]] == 4)
		result[0]--;
	current_step = result;
	return result;
}

long int get_direction(long long int x, long long int y) {
	return grid[x][y][0];
}

long int get_hit_count(long long int x, long long int y) {
	return grid[x][y][1];
}

long long int get_n() {
	return grid_n;
}

long long int get_center() {
	return center;
}

long long int *get_current_step() {
	return current_step;
}

long int ***get_grid() {
	return grid;
}