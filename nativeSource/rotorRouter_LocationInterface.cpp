#include <jni.h>

#include "rotorRouter_LocationInterface.h"
#include "backend.h"

JNIEXPORT void JNICALL Java_rotorRouter_LocationInterface_init(JNIEnv *env, jobject jo) {
	malloc_grid(500);
}

JNIEXPORT void JNICALL Java_rotorRouter_LocationInterface_setDirections(JNIEnv *env, jobject jo, jshortArray arr) {
	jshort *shorts = env->GetShortArrayElements(arr, JNI_FALSE);
	set_dirs((int *) shorts, env->GetArrayLength(arr));
	env->ReleaseShortArrayElements(arr, shorts, JNI_ABORT);
}

JNIEXPORT jshortArray JNICALL Java_rotorRouter_LocationInterface_getDirections(JNIEnv *env, jobject jo) {
	jshortArray shorts = env->NewShortArray(get_num_dirs());
	env->SetShortArrayRegion(shorts, 0, get_num_dirs(), (jshort *) get_dirs());
	return shorts;
}

JNIEXPORT jlongArray JNICALL Java_rotorRouter_LocationInterface_walk(JNIEnv *env, jobject jo) {
	jlongArray longs = env->NewLongArray(2);
	env->SetLongArrayRegion(longs, 0, 2, (jlong *) walk());
	return longs;
}

JNIEXPORT jlongArray JNICALL Java_rotorRouter_LocationInterface_step(JNIEnv *env, jobject jo) {
	jlongArray longs = env->NewLongArray(3);
	long long int *current_step = get_current_step();
	if (current_step != NULL)
		env->SetLongArrayRegion(longs, 0, 3, (jlong *) step(current_step[0], current_step[1]));
	else
		env->SetLongArrayRegion(longs, 0, 3, (jlong *) step(get_center(), get_center()));
	return longs;
}

JNIEXPORT jint JNICALL Java_rotorRouter_LocationInterface_hitCount(JNIEnv *env, jobject jo, jlong x, jlong y) {
	return (jint) get_hit_count(x, y);
}

JNIEXPORT jint JNICALL Java_rotorRouter_LocationInterface_direction(JNIEnv *env, jobject jo, jlong x, jlong y) {
	return (jint) get_direction(x, y);
}