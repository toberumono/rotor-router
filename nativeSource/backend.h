int set_dirs(int *new_dirs, int new_num_dirs);

int *get_dirs();

int get_num_dirs();

int malloc_grid(long long int n);

int free_grid();

int print_grid();

long long int *walk();

long long int *step(long long int x, long long int y);

long int get_direction(long long int x, long long int y);

long int get_hit_count(long long int x, long long int y);

long long int get_n();

long long int get_center();

long long int *get_current_step();

long int ***get_grid();